create table account (
Id int identity(1,1),
email nvarchar(50),
acc_password nvarchar(10),
full_name nvarchar(30),
acc_role int,
primary key(Id)
);
create table request(
Id int identity(1,1),
acc_Id int,
note nvarchar(100),
request_status int,
primary key(Id),
foreign key(acc_Id) references account(Id)
);
create table building(
Id int identity(1,1),
building_name nvarchar(20),
primary key(Id)
);
create table room(
Id int identity(1,1),
room_name nvarchar(20),
building_Id int,
primary key(Id),
foreign key(building_Id) references building(Id)
);
create table request_detail
(Id int identity(1,1),
request_Id int,
room_Id int,
time_using int,
date_booked nvarchar(20),
primary key(Id),
foreign key(room_Id) references room(Id),
foreign key(request_Id) references request(Id)
);
