#pragma checksum "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0c616d481834b830c9f131b429c7f5da7547f632"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_ManagerAccount), @"mvc.1.0.view", @"/Views/Home/ManagerAccount.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 2 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
using FacilitiesOnlinBooking.Model;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0c616d481834b830c9f131b429c7f5da7547f632", @"/Views/Home/ManagerAccount.cshtml")]
    public class Views_Home_ManagerAccount : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<Account>>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<!DOCTYPE html>\r\n");
            WriteLiteral("<html>\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0c616d481834b830c9f131b429c7f5da7547f6323049", async() => {
                WriteLiteral(@"
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0c616d481834b830c9f131b429c7f5da7547f6324392", async() => {
                WriteLiteral("\r\n\r\n    <div style=\"display:flex; justify-content:space-between;\">\r\n        <h2>Quản lý tài khoản</h2>\r\n        <h2><a href=\"/Home/Index\">Trang chủ</a></h2>\r\n    </div>\r\n    <h2 style=\"color:red;\">");
#nullable restore
#line 30 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
                      Write(ViewBag.mess3);

#line default
#line hidden
#nullable disable
                WriteLiteral("</h2>\r\n    <table>\r\n        <tr>\r\n            <th>Email</th>\r\n            <th>Họ và tên</th>\r\n            <th>Chức vụ</th>\r\n            <th>Xóa tài khoản</th>\r\n            <th>Phân quyền tài khoản</th>\r\n        </tr>\r\n");
#nullable restore
#line 39 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
          
            foreach (Account a in ViewBag.listAccount)
            {

#line default
#line hidden
#nullable disable
                WriteLiteral("                <tr>\r\n                    <td>");
#nullable restore
#line 43 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
                   Write(a.email);

#line default
#line hidden
#nullable disable
                WriteLiteral("</td>\r\n                    <td>");
#nullable restore
#line 44 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
                   Write(a.name);

#line default
#line hidden
#nullable disable
                WriteLiteral("</td>\r\n");
#nullable restore
#line 45 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
                      
                        if (a.role == 1)
                        {

#line default
#line hidden
#nullable disable
                WriteLiteral("                            <td>Admin</td>\r\n");
#nullable restore
#line 49 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
                        }
                        else if (a.role == 2)
                        {

#line default
#line hidden
#nullable disable
                WriteLiteral("                            <td>Staff</td>\r\n");
#nullable restore
#line 53 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
                        }
                        else
                        {

#line default
#line hidden
#nullable disable
                WriteLiteral("                            <td>Student</td>\r\n");
#nullable restore
#line 57 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
                        }
                    

#line default
#line hidden
#nullable disable
#nullable restore
#line 59 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
                      
                        if (ViewBag.role == 1)
                        {
                            if (a.role != 1)
                            {

#line default
#line hidden
#nullable disable
                WriteLiteral("                                <td><form action=\"/Home/DeleteAccount\" method=\"post\"><input type=\"text\" name=\"id\" style=\"display:none;\"");
                BeginWriteAttribute("value", " value=\"", 1869, "\"", 1882, 1);
#nullable restore
#line 64 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
WriteAttributeValue("", 1877, a.Id, 1877, 5, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" /><button type=\"submit\">Xóa</button></form></td>\r\n");
#nullable restore
#line 65 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
                            }
                            else
                            {

#line default
#line hidden
#nullable disable
                WriteLiteral("                                <td></td>\r\n");
#nullable restore
#line 69 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
                            }
                        }
                        else
                        {

#line default
#line hidden
#nullable disable
                WriteLiteral("                            <td>Chức năng cho Admin</td>\r\n");
#nullable restore
#line 74 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
                        }
                        if (ViewBag.role == 1)
                        {
                            if (a.role != 1)
                            {

#line default
#line hidden
#nullable disable
                WriteLiteral("                                <td><a");
                BeginWriteAttribute("href", " href=\"", 2463, "\"", 2494, 2);
                WriteAttributeValue("", 2470, "/Home/PhanQuyen?id=", 2470, 19, true);
#nullable restore
#line 79 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
WriteAttributeValue("", 2489, a.Id, 2489, 5, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(">Phân quyền</a></td>\r\n");
#nullable restore
#line 80 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
                            }
                            else
                            {

#line default
#line hidden
#nullable disable
                WriteLiteral("                                <td></td>\r\n");
#nullable restore
#line 84 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
                            }
                        }
                        else
                        {

#line default
#line hidden
#nullable disable
                WriteLiteral("                            <td>Chức năng cho Admin</td>\r\n");
#nullable restore
#line 89 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
                        }

                    

#line default
#line hidden
#nullable disable
                WriteLiteral("                </tr>\r\n");
#nullable restore
#line 93 "C:\Users\VACORN\OneDrive\Desktop\fob\FacilitiesOnlinBooking\FacilitiesOnlinBooking\Views\Home\ManagerAccount.cshtml"
            }
        

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    </table>\r\n\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</html>\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<Account>> Html { get; private set; }
    }
}
#pragma warning restore 1591
