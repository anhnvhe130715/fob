﻿using FacilitiesOnlinBooking.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FacilitiesOnlinBooking.Controller
{
    public class BuildingController : Microsoft.AspNetCore.Mvc.Controller
    {
        public IActionResult AddBuilding()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddBuilding(Building building)
        {
            return View();
        }
        public IActionResult UpdateBuilding()
        {
            return View();
        }
        [HttpPut]
        public IActionResult UpdateBuilding(Building building)
        {
            return View();
        }
        [HttpDelete]
        public IActionResult DeleteBuilding(Building building)
        {
            return View();
        }
    }
}
